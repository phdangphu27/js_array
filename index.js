const inputEl = document.getElementById("input");
const arrEl = document.getElementById("arr");
const arr = [];

const add = () => {
  const inputVal = +inputEl.value;

  arr.push(inputVal);

  arrEl.innerHTML = `[${arr}]`;

  inputEl.value = "";
};

// exercise 1

const result1El = document.getElementById("result1");

const calc = () => {
  let sum = 0;
  const newArr = arr.filter((a) => a > 0);
  newArr.forEach((n) => {
    sum += n;
  });

  result1El.innerHTML = sum;
};

// exercise 2
const result2El = document.getElementById("result2");

const check = () => {
  const newArr = arr.filter((a) => {
    return a > 0;
  });

  result2El.innerHTML = `[${newArr}]`;
};

// exercise 3
const result3El = document.getElementById("result3");

const findMin = () => {
  const copyArr3 = arr;
  copyArr3.sort((a, b) => a - b);

  result3El.innerHTML = `${copyArr3[0]}`;
};

// exercise 4
const result4El = document.getElementById("result4");

const checkFn = () => {
  const newArr = arr.filter((a) => a > 0).sort((a, b) => a - b);

  result4El.innerHTML = `${newArr[0]}`;
};

// exercise 5
const result5El = document.getElementById("result5");

const checkLastEven = () => {
  const newArr = arr.filter((a) => {
    return a % 2 === 0 && a !== 0;
  });

  const lastIndex = newArr.length - 1;
  const noEven = newArr[lastIndex] === undefined ? -1 : newArr[lastIndex];

  result5El.innerHTML = `Last even: ${noEven}`;
};

// exercise 6
const fromEl = document.getElementById("from");
const toEl = document.getElementById("to");
const result6El = document.getElementById("result6");

const swap = () => {
  const fromVal = +fromEl.value;
  const toVal = +toEl.value;
  const arr6 = arr;

  const a = arr6[fromVal];
  arr6[fromVal] = arr6[toVal];
  arr6[toVal] = a;

  result6El.innerHTML = `<p>Swapped array: [${arr6}]</p>`;
};

// exercise 7
const result7El = document.getElementById("result7");

const ascending = () => {
  const copyArr7 = arr;

  copyArr7.sort((a, b) => a - b);

  result7El.innerHTML = `[${copyArr7}]`;
};

// exercise 8
const result8El = document.getElementById("result8");

const firstPrimeNum = () => {
  const newArr = arr.filter((a) => {
    let app = 0;
    for (let i = 1; i <= a; i++) {
      if (a % i === 0) {
        app++;
      }
    }
    if (app === 2) {
      return a;
    }
  });

  const check = newArr[0] === undefined ? -1 : newArr[0];

  result8El.innerHTML = `${
    check < 0
      ? `No prime number found -1`
      : `<p>First prime number: ${check}</p>`
  }`;
};

// exercise 9
const result9El = document.getElementById("result9");

const checkInteger = () => {
  let count = 0;

  arr.forEach((num) => {
    if (Number.isInteger(num)) {
      count++;
    }
  });

  result9El.innerHTML = `Number of integers: ${count}`;
};

// exercise 10
const result10El = document.getElementById("result10");

const moreOrLess = () => {
  let positive = 0;
  let negative = 0;

  arr.forEach((num) => {
    if (num > 0) {
      positive++;
    }
    if (num < 0) {
      negative++;
    }
  });

  const check = () => {
    if (positive === negative) {
      return "there are equal positive numbers and negative";
    } else if (positive > negative) {
      return "there are more positive numbers than negative";
    } else {
      return "there are less positive numbers and negative";
    }
  };

  result10El.innerHTML = `${check()}`;
};
